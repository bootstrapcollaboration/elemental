/*
   Copyright (c) 2009-2016, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
#include <El-lite.hpp>
#ifdef EL_HAVE_GMPXX

namespace El {

int BigFloat::Sign() const
{ return sgn(gmp_float); }

mp_bitcnt_t BigFloat::Precision() const
{ return gmp_float.get_prec(); }

void BigFloat::SetPrecision( mp_bitcnt_t prec )
{
    gmp_float.set_prec(prec);
}

// GMP does not support conversion from long double, long long, or
// unsigned long long
BigFloat::BigFloat( const BigFloat& a, mp_bitcnt_t prec )
    : gmp_float(a.gmp_float,prec) {}

BigFloat::BigFloat( const unsigned& a, mp_bitcnt_t prec )
    : gmp_float(a,prec) {}

BigFloat::BigFloat( const unsigned long& a, mp_bitcnt_t prec )
    : gmp_float(a,prec) {}

BigFloat::BigFloat( const unsigned long long& a, mp_bitcnt_t prec )
    : gmp_float(static_cast<unsigned long>(a),prec) {}

BigFloat::BigFloat( const int& a, mp_bitcnt_t prec )
    : gmp_float(a,prec) {}

BigFloat::BigFloat( const long int& a, mp_bitcnt_t prec )
    : gmp_float(a,prec) {}

BigFloat::BigFloat( const long long& a, mp_bitcnt_t prec )
    : gmp_float(static_cast<long int>(a),prec) {}

BigFloat::BigFloat( const float& a, mp_bitcnt_t prec )
    : gmp_float(a,prec) {}

BigFloat::BigFloat( const double& a, mp_bitcnt_t prec )
    : gmp_float(a,prec) {}

BigFloat::BigFloat( const long double& a, mp_bitcnt_t prec )
    : gmp_float(static_cast<double>(a),prec) {}

BigFloat::BigFloat( const char* str, int base, mp_bitcnt_t prec )
  : gmp_float(str,prec,base) {}

BigFloat::BigFloat( const std::string& str, int base, mp_bitcnt_t prec )
  : gmp_float(str,prec,base) {}

void BigFloat::Zero()
{
    gmp_float=0;
}

BigFloat& BigFloat::operator=( const unsigned& a )
{
    gmp_float=a;
    return *this;
}

BigFloat& BigFloat::operator=( const unsigned long& a )
{
    gmp_float=a;
    return *this;
}

BigFloat& BigFloat::operator=( const int& a )
{
    gmp_float=a;
    return *this;
}

BigFloat& BigFloat::operator=( const long int& a )
{
    gmp_float=a;
    return *this;
}

BigFloat& BigFloat::operator=( const float& a )
{
    gmp_float=a;
    return *this;
}

BigFloat& BigFloat::operator=( const double& a )
{
    gmp_float=a;
    return *this;
}


BigFloat& BigFloat::operator+=( const unsigned& a )
{
    gmp_float+=a;
    return *this;
}

BigFloat& BigFloat::operator+=( const unsigned long& a )
{
    gmp_float+=a;
    return *this;
}

BigFloat& BigFloat::operator+=( const int& a )
{
    gmp_float+=a;
    return *this;
}

BigFloat& BigFloat::operator+=( const long int& a )
{
    gmp_float+=a;
    return *this;
}

BigFloat& BigFloat::operator+=( const float& a )
{
    gmp_float+=a;
    return *this;
}

BigFloat& BigFloat::operator+=( const double& a )
{
    gmp_float+=a;
    return *this;
}

BigFloat& BigFloat::operator+=( const BigFloat& a )
{
    gmp_float+=a.gmp_float;
    return *this;
}

BigFloat& BigFloat::operator-=( const unsigned& a )
{
    gmp_float-=a;
    return *this;
}

BigFloat& BigFloat::operator-=( const unsigned long& a )
{
    gmp_float-=a;
    return *this;
}

BigFloat& BigFloat::operator-=( const int& a )
{
    gmp_float-=a;
    return *this;
}

BigFloat& BigFloat::operator-=( const long int& a )
{
    gmp_float-=a;
    return *this;
}

BigFloat& BigFloat::operator-=( const float& a )
{
    gmp_float-=a;
    return *this;
}

BigFloat& BigFloat::operator-=( const double& a )
{
    gmp_float-=a;
    return *this;
}

BigFloat& BigFloat::operator-=( const BigFloat& a )
{
    gmp_float-=a.gmp_float;
    return *this;
}

BigFloat& BigFloat::operator++()
{
    *this += 1;
    return *this;
}

BigFloat BigFloat::operator++(int)
{
    BigFloat result(*this);
    ++(*this);
    return result;
}

BigFloat& BigFloat::operator--()
{
    *this -= 1;
    return *this;
}

BigFloat BigFloat::operator--(int)
{
    BigFloat result(*this);
    --(*this);
    return result;
}

BigFloat& BigFloat::operator*=( const unsigned& a )
{
    gmp_float*=a;
    return *this;
}

BigFloat& BigFloat::operator*=( const unsigned long& a )
{
    gmp_float*=a;
    return *this;
}

BigFloat& BigFloat::operator*=( const int& a )
{
    gmp_float*=a;
    return *this;
}

BigFloat& BigFloat::operator*=( const long int& a )
{
    gmp_float*=a;
    return *this;
}

BigFloat& BigFloat::operator*=( const float& a )
{
    gmp_float*=a;
    return *this;
}

BigFloat& BigFloat::operator*=( const double& a )
{
    gmp_float*=a;
    return *this;
}

BigFloat& BigFloat::operator*=( const BigFloat& a )
{
    gmp_float*=a.gmp_float;
    return *this;
}

BigFloat& BigFloat::operator/=( const unsigned& a )
{
    gmp_float/=a;
    return *this;
}

BigFloat& BigFloat::operator/=( const unsigned long& a )
{
    gmp_float/=a;
    return *this;
}

BigFloat& BigFloat::operator/=( const int& a )
{
    gmp_float/=a;
    return *this;
}

BigFloat& BigFloat::operator/=( const long int& a )
{
    gmp_float/=a;
    return *this;
}

BigFloat& BigFloat::operator/=( const float& a )
{
    gmp_float/=a;
    return *this;
}

BigFloat& BigFloat::operator/=( const double& a )
{
    gmp_float/=a;
    return *this;
}

BigFloat& BigFloat::operator/=( const BigFloat& a )
{
    gmp_float/=a.gmp_float;
    return *this;
}

BigFloat BigFloat::operator-() const
{
    BigFloat result(*this);
    result.gmp_float=-result.gmp_float;
    return result;
}

BigFloat BigFloat::operator+() const
{
    return *this;
}

// The built-in mpf_class::operator<<= and >>= do not correctly handle
// negative numbers.  So we check and invert sign if needed.
BigFloat& BigFloat::operator<<=( const int& a )
{
    if(a>=0)
      {
        gmp_float<<=a;
      }
    else
      {
        gmp_float>>=-a;
      }
    return *this;
}

BigFloat& BigFloat::operator<<=( const long int& a )
{
    if(a>=0)
      {
        gmp_float<<=a;
      }
    else
      {
        gmp_float>>=-a;
      }
    return *this;
}

BigFloat& BigFloat::operator<<=( const unsigned& a )
{
    gmp_float<<=a;
    return *this;
}

BigFloat& BigFloat::operator<<=( const long unsigned& a )
{
    gmp_float<<=a;
    return *this;
}

BigFloat& BigFloat::operator>>=( const int& a )
{
    if(a>=0)
      {
        gmp_float>>=a;
      }
    else
      {
        gmp_float>>=-a;
      }
    return *this;
}

BigFloat& BigFloat::operator>>=( const long int& a )
{
    if(a>=0)
      {
        gmp_float>>=a;
      }
    else
      {
        gmp_float>>=-a;
      }
    return *this;
}

BigFloat& BigFloat::operator>>=( const unsigned& a )
{
    gmp_float>>=a;
    return *this;
}

BigFloat& BigFloat::operator>>=( const long unsigned& a )
{
    gmp_float>>=a;
    return *this;
}

BigFloat::operator bool() const
{ return gmp_float!=0; }

BigFloat::operator int() const
{ return gmp_float.get_si(); }

BigFloat::operator long () const
{ return gmp_float.get_si(); }

BigFloat::operator long long() const
{ return gmp_float.get_si(); }

BigFloat::operator unsigned() const
{ return gmp_float.get_ui(); }

BigFloat::operator unsigned long() const
{ return gmp_float.get_ui(); }

BigFloat::operator unsigned long long() const
{ return gmp_float.get_ui(); }

BigFloat::operator float() const
{ return gmp_float.get_d(); }

BigFloat::operator double() const
{ return gmp_float.get_d(); }

BigFloat::operator long double() const
{ return gmp_float.get_d(); }

size_t BigFloat::SerializedSize() const
{
    return sizeof(int)+
      sizeof(mp_exp_t)+
      sizeof(mp_limb_t)*gmp::num_limbs;
}

byte* BigFloat::Serialize( byte* buf ) const
{
    // NOTE: Do not serialize precision.  It should be set by the
    // constructor, and if we are mixing precision, we are going to
    // have a bad day anyway.

    std::memcpy( buf, &(gmp_float.get_mpf_t()[0]._mp_size), sizeof(int) );
    buf += sizeof(int);
    std::memcpy( buf, &(gmp_float.get_mpf_t()[0]._mp_exp), sizeof(mp_exp_t) );
    buf += sizeof(mp_exp_t);

    // GMP uses negative sizes to indicate sign
    std::memcpy( buf, gmp_float.get_mpf_t()[0]._mp_d,
                 std::abs(gmp_float.get_mpf_t()[0]._mp_size)*sizeof(mp_limb_t) );
    buf += gmp::num_limbs*sizeof(mp_limb_t);
    return buf;
}

BigFloat operator+( const BigFloat& a, const BigFloat& b )
{ return BigFloat(a) += b; }
BigFloat operator-( const BigFloat& a, const BigFloat& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const BigFloat& a, const BigFloat& b )
{ return BigFloat(a) *= b; }
BigFloat operator/( const BigFloat& a, const BigFloat& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const BigFloat& a, const unsigned& b )
{ return BigFloat(a) += b; }
BigFloat operator-( const BigFloat& a, const unsigned& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const BigFloat& a, const unsigned& b )
{ return BigFloat(a) *= b; }
BigFloat operator/( const BigFloat& a, const unsigned& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const BigFloat& a, const unsigned long& b )
{ return BigFloat(a) += b; }
BigFloat operator-( const BigFloat& a, const unsigned long& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const BigFloat& a, const unsigned long& b )
{ return BigFloat(a) *= b; }
BigFloat operator/( const BigFloat& a, const unsigned long& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const BigFloat& a, const int& b )
{ return BigFloat(a) += b; }
BigFloat operator-( const BigFloat& a, const int& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const BigFloat& a, const int& b )
{ return BigFloat(a) *= b; }
BigFloat operator/( const BigFloat& a, const int& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const BigFloat& a, const long int& b )
{ return BigFloat(a) += b; }
BigFloat operator-( const BigFloat& a, const long int& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const BigFloat& a, const long int& b )
{ return BigFloat(a) *= b; }
BigFloat operator/( const BigFloat& a, const long int& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const BigFloat& a, const float& b )
{ return BigFloat(a) += b; }
BigFloat operator-( const BigFloat& a, const float& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const BigFloat& a, const float& b )
{ return BigFloat(a) *= b; }
BigFloat operator/( const BigFloat& a, const float& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const BigFloat& a, const double& b )
{ return BigFloat(a) += b; }
BigFloat operator-( const BigFloat& a, const double& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const BigFloat& a, const double& b )
{ return BigFloat(a) *= b; }
BigFloat operator/( const BigFloat& a, const double& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const unsigned& a, const BigFloat& b )
{ return BigFloat(b) += a; }
BigFloat operator-( const unsigned& a, const BigFloat& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const unsigned& a, const BigFloat& b )
{ return BigFloat(b) *= a; }
BigFloat operator/( const unsigned& a, const BigFloat& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const unsigned long& a, const BigFloat& b )
{ return BigFloat(b) += a; }
BigFloat operator-( const unsigned long& a, const BigFloat& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const unsigned long& a, const BigFloat& b )
{ return BigFloat(b) *= a; }
BigFloat operator/( const unsigned long& a, const BigFloat& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const int& a, const BigFloat& b )
{ return BigFloat(b) += a; }
BigFloat operator-( const int& a, const BigFloat& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const int& a, const BigFloat& b )
{ return BigFloat(b) *= a; }
BigFloat operator/( const int& a, const BigFloat& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const long int& a, const BigFloat& b )
{ return BigFloat(b) += a; }
BigFloat operator-( const long int& a, const BigFloat& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const long int& a, const BigFloat& b )
{ return BigFloat(b) *= a; }
BigFloat operator/( const long int& a, const BigFloat& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const float& a, const BigFloat& b )
{ return BigFloat(b) += a; }
BigFloat operator-( const float& a, const BigFloat& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const float& a, const BigFloat& b )
{ return BigFloat(b) *= a; }
BigFloat operator/( const float& a, const BigFloat& b )
{ return BigFloat(a) /= b; }

BigFloat operator+( const double& a, const BigFloat& b )
{ return BigFloat(b) += a; }
BigFloat operator-( const double& a, const BigFloat& b )
{ return BigFloat(a) -= b; }
BigFloat operator*( const double& a, const BigFloat& b )
{ return BigFloat(b) *= a; }
BigFloat operator/( const double& a, const BigFloat& b )
{ return BigFloat(a) /= b; }

BigFloat operator<<( const BigFloat& a, const int& b )
{ return BigFloat(a) <<= b; }
BigFloat operator<<( const BigFloat& a, const long int& b )
{ return BigFloat(a) <<= b; }
BigFloat operator<<( const BigFloat& a, const unsigned& b )
{ return BigFloat(a) <<= b; }
BigFloat operator<<( const BigFloat& a, const long unsigned& b )
{ return BigFloat(a) <<= b; }

BigFloat operator>>( const BigFloat& a, const int& b )
{ return BigFloat(a) >>= b; }
BigFloat operator>>( const BigFloat& a, const long int& b )
{ return BigFloat(a) >>= b; }
BigFloat operator>>( const BigFloat& a, const unsigned& b )
{ return BigFloat(a) >>= b; }
BigFloat operator>>( const BigFloat& a, const long unsigned& b )
{ return BigFloat(a) >>= b; }

bool operator<( const BigFloat& a, const BigFloat& b )
{ return a.gmp_float < b.gmp_float; }

bool operator>( const BigFloat& a, const BigFloat& b )
{ return a.gmp_float > b.gmp_float; }

bool operator<=( const BigFloat& a, const BigFloat& b )
{ return a.gmp_float <= b.gmp_float; }

bool operator>=( const BigFloat& a, const BigFloat& b )
{ return a.gmp_float >= b.gmp_float; }

bool operator==( const BigFloat& a, const BigFloat& b )
{ return a.gmp_float == b.gmp_float; }

bool operator!=( const BigFloat& a, const BigFloat& b )
{ return a.gmp_float != b.gmp_float; }

std::ostream& operator<<( std::ostream& os, const BigFloat& alpha )
{
    os << alpha.gmp_float;
    return os;
}

std::istream& operator>>( std::istream& is, BigFloat& alpha )
{
    is >> alpha.gmp_float;
    return is;
}

} // namespace El

#endif // ifdef EL_HAVE_GMPXX
