# Try to find the GMPXX library
# https://gmplib.org/
#
# This module supports requiring a minimum version, e.g. you can do
#   find_package(GMPXX 6.0.0)
# to require version 6.0.0 to newer of GMPXX.
#
# Once done this will define
#
#  GMPXX_FOUND - system has GMPXX lib with correct version
#  GMPXX_INCLUDES - the GMPXX include directory
#  GMPXX_LIBRARIES - the GMPXX library
#  GMPXX_VERSION - GMPXX version
#
# Copyright (c) 2016 Jack Poulson, <jack.poulson@gmail.com>
# Redistribution and use is allowed according to the terms of the BSD license.

find_path(GMPXX_INCLUDES NAMES gmpxx.h PATHS $ENV{GMPXXDIR} $ENV{GMPXX_HOME} $ENV{GMPXX_INCLUDE} $ENV{GMPDIR} $ENV{GMP_HOME} $ENV{GMP_INCLUDE} ${INCLUDE_INSTALL_DIR})
find_library(GMPXX_LIBRARIES gmpxx PATHS $ENV{GMPXXDIR} $ENV{GMPXX_HOME} $ENV{GMPXX_LIB} $ENV{GMPDIR} $ENV{GMP_HOME} $ENV{GMP_LIB} ${LIB_INSTALL_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GMPXX DEFAULT_MSG
                                  GMPXX_INCLUDES GMPXX_LIBRARIES)
mark_as_advanced(GMPXX_INCLUDES GMPXX_LIBRARIES)
