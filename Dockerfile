# syntax=docker/dockerfile:1

# latest alpine release
FROM alpine:3.18 AS build

RUN apk add \
    cmake \
    g++ \
    git \
    make \
    mpfr-dev \
    gmp-dev \
    openblas-dev \
    openmpi \
    openmpi-dev \
    openssh
WORKDIR /usr/local/src/elemental
# Build Elemental from current sources
COPY . .
RUN mkdir -p build && \
    cd build && \
    cmake .. -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc  \
             -DCMAKE_INSTALL_PREFIX=/usr/local/ \
             -DEL_TESTS=ON && \
    make -j`nproc` && \
    make -j`nproc` install
# If build crashes, try replacing "make -j`nproc`" with a (single-threaded) "make"

# Take only Elemental headers and binaries + load necessary dynamic libraries
FROM alpine:3.18 as install
RUN apk add \
    gmp \
    libgmpxx \
    libstdc++ \
    mpfr \
    openblas \
    openmpi \
    openssh
COPY --from=build /usr/local/bin /usr/local/bin
COPY --from=build /usr/local/include /usr/local/include
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/CMake /usr/local/CMake

# Test target, usage:
# docker build . --tag elemental-test --target test
# docker run elemental-test ctest --output-on-failure --no-tests=error
FROM install as test
RUN apk add cmake # for ctest
COPY --from=build /usr/local/src/elemental/build /usr/local/src/elemental/build
WORKDIR /usr/local/src/elemental/build

FROM install