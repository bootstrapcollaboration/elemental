/*
   Copyright (c) 2009-2016, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
#pragma once

#include <gmpxx.h>

namespace El {

namespace gmp {

extern size_t num_limbs;

mp_bitcnt_t Precision();
void SetPrecision( mp_bitcnt_t precision );

int NumIntBits();
int NumIntLimbs();
void SetMinIntBits( int minIntBits );

// NOTE: These should only be called internally
void RegisterMPI();
void FreeMPI();

}


class BigInt
{
private:
    mpz_t mpzInt_;

    void Init( int numBits=gmp::NumIntBits() );

public:
    mpz_ptr Pointer();
    mpz_srcptr LockedPointer() const;
    void SetNumLimbs( int numLimbs );
    void SetMinBits( int minBits );
    int NumLimbs() const;
    int NumBits() const;

    BigInt();
    BigInt( const BigInt& a, int numBits=gmp::NumIntBits() );
    BigInt( const unsigned& a, int numBits=gmp::NumIntBits() );
    BigInt( const unsigned long& a, int numBits=gmp::NumIntBits() );
    BigInt( const unsigned long long& a, int numBits=gmp::NumIntBits() );
    BigInt( const int& a, int numBits=gmp::NumIntBits() );
    BigInt( const long int& a, int numBits=gmp::NumIntBits() );
    BigInt( const long long int& a, int numBits=gmp::NumIntBits() );
    BigInt( const double& a, int numBits=gmp::NumIntBits() );
    BigInt( const char* str, int base, int numBits=gmp::NumIntBits() );
    BigInt( const std::string& str, int base, int numBits=gmp::NumIntBits() );
    BigInt( BigInt&& a );
    ~BigInt();

    void Zero();

    BigInt& operator=( const BigInt& a );
    BigInt& operator=( const unsigned& a );
    BigInt& operator=( const unsigned long& a );
    BigInt& operator=( const unsigned long long& a );
    BigInt& operator=( const int& a );
    BigInt& operator=( const long int& a );
    BigInt& operator=( const long long int& a );
    BigInt& operator=( const double& a );
    BigInt& operator=( BigInt&& a );

    BigInt& operator+=( const int& a );
    BigInt& operator+=( const long int& a );
    BigInt& operator+=( const long long int& a );
    BigInt& operator+=( const unsigned& a );
    BigInt& operator+=( const unsigned long& a );
    BigInt& operator+=( const unsigned long long& a );
    BigInt& operator+=( const BigInt& a );

    BigInt& operator-=( const int& a );
    BigInt& operator-=( const long int& a );
    BigInt& operator-=( const long long int& a );
    BigInt& operator-=( const unsigned& a );
    BigInt& operator-=( const unsigned long& a );
    BigInt& operator-=( const unsigned long long& a );
    BigInt& operator-=( const BigInt& a );

    BigInt& operator++();
    BigInt  operator++(int);
    BigInt& operator--();
    BigInt  operator--(int);

    BigInt& operator*=( const int& a );
    BigInt& operator*=( const long int& a );
    BigInt& operator*=( const long long int& a );
    BigInt& operator*=( const unsigned& a );
    BigInt& operator*=( const unsigned long& a );
    BigInt& operator*=( const unsigned long long& a );
    BigInt& operator*=( const BigInt& a );

    BigInt& operator/=( const BigInt& a );

    // Negation
    BigInt operator-() const;

    // Identity map
    BigInt operator+() const;

    // Overwrite the input modulo b
    BigInt& operator%=( const BigInt& b );
    BigInt& operator%=( const unsigned& b );
    BigInt& operator%=( const unsigned long& b );
    BigInt& operator%=( const unsigned long long& b );

    // Analogue of bit-shifting left
    BigInt& operator<<=( const unsigned& a ); 
    BigInt& operator<<=( const unsigned long& a );

    // Analogue of bit-shifting right
    BigInt& operator>>=( const unsigned& a ); 
    BigInt& operator>>=( const unsigned long& a );

    // Casting
    explicit operator bool() const;
    explicit operator int() const;
    explicit operator long() const;
    explicit operator long long() const;
    explicit operator unsigned() const;
    explicit operator unsigned long() const;
    explicit operator unsigned long long() const;
    explicit operator float() const;
    explicit operator double() const;
    explicit operator long double() const;
#ifdef EL_HAVE_QUAD
    explicit operator Quad() const;
#endif
#ifdef EL_HAVE_QD
    explicit operator DoubleDouble() const;
    explicit operator QuadDouble() const;
#endif

    size_t SerializedSize( int numLimbs=gmp::NumIntLimbs() ) const;
          byte* Serialize( byte* buf, int numLimbs=gmp::NumIntLimbs() ) const;
          byte* Deserialize( byte* buf, int numLimbs=gmp::NumIntLimbs() );
    const byte* Deserialize( const byte* buf, int numLimbs=gmp::NumIntLimbs() );

    size_t ThinSerializedSize() const;
          byte* ThinSerialize( byte* buf ) const;
          byte* ThinDeserialize( byte* buf );
    const byte* ThinDeserialize( const byte* buf );
};

BigInt operator+( const BigInt& a, const BigInt& b );
BigInt operator-( const BigInt& a, const BigInt& b );
BigInt operator*( const BigInt& a, const BigInt& b );
BigInt operator/( const BigInt& a, const BigInt& b );

BigInt operator+( const BigInt& a, const unsigned& b );
BigInt operator-( const BigInt& a, const unsigned& b );
BigInt operator*( const BigInt& a, const unsigned& b );
BigInt operator/( const BigInt& a, const unsigned& b );

BigInt operator+( const BigInt& a, const unsigned long& b );
BigInt operator-( const BigInt& a, const unsigned long& b );
BigInt operator*( const BigInt& a, const unsigned long& b );
BigInt operator/( const BigInt& a, const unsigned long& b );

BigInt operator+( const BigInt& a, const unsigned long long& b );
BigInt operator-( const BigInt& a, const unsigned long long& b );
BigInt operator*( const BigInt& a, const unsigned long long& b );
BigInt operator/( const BigInt& a, const unsigned long long& b );

BigInt operator+( const BigInt& a, const int& b );
BigInt operator-( const BigInt& a, const int& b );
BigInt operator*( const BigInt& a, const int& b );
BigInt operator/( const BigInt& a, const int& b );

BigInt operator+( const BigInt& a, const long int& b );
BigInt operator-( const BigInt& a, const long int& b );
BigInt operator*( const BigInt& a, const long int& b );
BigInt operator/( const BigInt& a, const long int& b );

BigInt operator+( const BigInt& a, const long long int& b );
BigInt operator-( const BigInt& a, const long long int& b );
BigInt operator*( const BigInt& a, const long long int& b );
BigInt operator/( const BigInt& a, const long long int& b );

BigInt operator+( const unsigned& a, const BigInt& b );
BigInt operator-( const unsigned& a, const BigInt& b );
BigInt operator*( const unsigned& a, const BigInt& b );
BigInt operator/( const unsigned& a, const BigInt& b );

BigInt operator+( const unsigned long& a, const BigInt& b );
BigInt operator-( const unsigned long& a, const BigInt& b );
BigInt operator*( const unsigned long& a, const BigInt& b );
BigInt operator/( const unsigned long& a, const BigInt& b );

BigInt operator+( const unsigned long long& a, const BigInt& b );
BigInt operator-( const unsigned long long& a, const BigInt& b );
BigInt operator*( const unsigned long long& a, const BigInt& b );
BigInt operator/( const unsigned long long& a, const BigInt& b );

BigInt operator+( const int& a, const BigInt& b );
BigInt operator-( const int& a, const BigInt& b );
BigInt operator*( const int& a, const BigInt& b );
BigInt operator/( const int& a, const BigInt& b );

BigInt operator+( const long int& a, const BigInt& b );
BigInt operator-( const long int& a, const BigInt& b );
BigInt operator*( const long int& a, const BigInt& b );
BigInt operator/( const long int& a, const BigInt& b );

BigInt operator+( const long long int& a, const BigInt& b );
BigInt operator-( const long long int& a, const BigInt& b );
BigInt operator*( const long long int& a, const BigInt& b );
BigInt operator/( const long long int& a, const BigInt& b );

BigInt operator%( const BigInt& a, const BigInt& b );
unsigned operator%( const BigInt& a, const unsigned& b );
unsigned long operator%( const BigInt& a, const unsigned long& b );
unsigned long long operator%( const BigInt& a, const unsigned long long& b );

BigInt operator<<( const BigInt& a, const int& b );
BigInt operator<<( const BigInt& a, const long int& b );
BigInt operator<<( const BigInt& a, const unsigned& b );
BigInt operator<<( const BigInt& a, const unsigned long& b );
BigInt operator<<( const BigInt& a, const unsigned long long& b );

BigInt operator>>( const BigInt& a, const int& b );
BigInt operator>>( const BigInt& a, const long int& b );
BigInt operator>>( const BigInt& a, const unsigned& b );
BigInt operator>>( const BigInt& a, const unsigned long& b );
BigInt operator>>( const BigInt& a, const unsigned long long& b );

bool operator<( const BigInt& a, const BigInt& b );
bool operator<( const BigInt& a, const int& b );
bool operator<( const BigInt& a, const long int& b );
bool operator<( const BigInt& a, const long long int& b );
bool operator<( const int& a, const BigInt& b );
bool operator<( const long int& a, const BigInt& b );

bool operator<( const long long int& a, const BigInt& b );
bool operator>( const BigInt& a, const BigInt& b );
bool operator>( const BigInt& a, const int& b );
bool operator>( const BigInt& a, const long int& b );
bool operator>( const BigInt& a, const long long int& b );
bool operator>( const int& a, const BigInt& b );
bool operator>( const long int& a, const BigInt& b );
bool operator>( const long long int& a, const BigInt& b );

bool operator<=( const BigInt& a, const BigInt& b );
bool operator<=( const BigInt& a, const int& b );
bool operator<=( const BigInt& a, const long int& b );
bool operator<=( const BigInt& a, const long long int& b );
bool operator<=( const int& a, const BigInt& b );
bool operator<=( const long int& a, const BigInt& b );
bool operator<=( const long long int& a, const BigInt& b );

bool operator>=( const BigInt& a, const BigInt& b );
bool operator>=( const BigInt& a, const int& b );
bool operator>=( const BigInt& a, const long int& b );
bool operator>=( const BigInt& a, const long long int& b );
bool operator>=( const int& a, const BigInt& b );
bool operator>=( const long int& a, const BigInt& b );
bool operator>=( const long long int& a, const BigInt& b );

bool operator==( const BigInt& a, const BigInt& b );
bool operator==( const BigInt& a, const int& b );
bool operator==( const BigInt& a, const long int& b );
bool operator==( const BigInt& a, const long long int& b );
bool operator==( const int& a, const BigInt& b );
bool operator==( const long int& a, const BigInt& b );
bool operator==( const long long int& a, const BigInt& b );

bool operator!=( const BigInt& a, const BigInt& b );
bool operator!=( const BigInt& a, const int& b );
bool operator!=( const BigInt& a, const long int& b );
bool operator!=( const BigInt& a, const long long int& b );
bool operator!=( const int& a, const BigInt& b );
bool operator!=( const long int& a, const BigInt& b );
bool operator!=( const long long int& a, const BigInt& b );

std::ostream& operator<<( std::ostream& os, const BigInt& alpha );
std::istream& operator>>( std::istream& is,       BigInt& alpha );

// The following constants are provided to avoid needlessly worrying about
// allocating them as BigInt's within number theoretic routines (such as 
// Miller-Rabin and Pollard's (p-1) algorithm)
const BigInt& BigIntZero();
const BigInt& BigIntOne();
const BigInt& BigIntTwo();


// The following thus provides a minimal wrapper for providing value
// semantics for mpf_class.
//
// This API is extremely loosely related to that of Pavel Holoborodko's
// MPFR C++, which was not used within Elemental for a variety of 
// idiosyncratic reasons.

class BigFloat {
public:
    mpf_class gmp_float;

    int Sign() const;
    mp_bitcnt_t Precision() const;
    void        SetPrecision( mp_bitcnt_t );
    size_t      NumLimbs() const
    {
      return gmp::num_limbs;
    }

    // NOTE: The default constructor does not take an mp_bitcnt_t as input
    //       due to the ambiguity is would cause with respect to the
    //       constructors which accept an integer and an (optional) precision
    BigFloat() : gmp_float(0,gmp::Precision()) {}
    BigFloat( const BigFloat& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const unsigned& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const unsigned long& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const unsigned long long& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const int& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const long int& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const long long& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const float& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const double& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const long double& a, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( const char* str, int base=10, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat
    ( const std::string& str, int base=10, mp_bitcnt_t prec=gmp::Precision() );
    BigFloat( BigFloat&& a ) = default;
    ~BigFloat() = default;

    void Zero();

    BigFloat& operator=( const BigFloat& a ) = default;
    BigFloat& operator=( const double& a );
    BigFloat& operator=( const float& a );
    BigFloat& operator=( const int& a );
    BigFloat& operator=( const long int& a );
    BigFloat& operator=( const unsigned& a );
    BigFloat& operator=( const unsigned long& a );
    BigFloat& operator=( BigFloat&& a ) = default;

    BigFloat& operator+=( const unsigned& a );
    BigFloat& operator+=( const unsigned long& a );
    BigFloat& operator+=( const int& a );
    BigFloat& operator+=( const long int& a );
    BigFloat& operator+=( const float& a );
    BigFloat& operator+=( const double& a );
    BigFloat& operator+=( const BigFloat& a );

    BigFloat& operator-=( const unsigned& a );
    BigFloat& operator-=( const unsigned long& a );
    BigFloat& operator-=( const int& a );
    BigFloat& operator-=( const long int& a );
    BigFloat& operator-=( const float& a );
    BigFloat& operator-=( const double& a );
    BigFloat& operator-=( const BigFloat& a );

    BigFloat& operator++();
    BigFloat  operator++(int);
    BigFloat& operator--();
    BigFloat  operator--(int);

    BigFloat& operator*=( const unsigned& a );
    BigFloat& operator*=( const unsigned long& a );
    BigFloat& operator*=( const int& a );
    BigFloat& operator*=( const long int& a );
    BigFloat& operator*=( const float& a );
    BigFloat& operator*=( const double& a );
    BigFloat& operator*=( const BigFloat& a );

    BigFloat& operator/=( const unsigned& a );
    BigFloat& operator/=( const unsigned long& a );
    BigFloat& operator/=( const int& a );
    BigFloat& operator/=( const long int& a );
    BigFloat& operator/=( const float& a );
    BigFloat& operator/=( const double& a );
    BigFloat& operator/=( const BigFloat& a );

    // Negation
    BigFloat operator-() const;

    // Identity map
    BigFloat operator+() const;

    // Analogue of bit-shifting left
    BigFloat& operator<<=( const int& a );
    BigFloat& operator<<=( const long int& a );
    BigFloat& operator<<=( const unsigned& a ); 
    BigFloat& operator<<=( const unsigned long& a );

    // Analogue of bit-shifting right
    BigFloat& operator>>=( const int& a );
    BigFloat& operator>>=( const long int& a );
    BigFloat& operator>>=( const unsigned& a ); 
    BigFloat& operator>>=( const unsigned long& a );

    // Casting
    explicit operator bool() const;
    explicit operator int() const;
    explicit operator long() const;
    explicit operator long long() const;
    explicit operator unsigned() const;
    explicit operator unsigned long() const;
    explicit operator unsigned long long() const;
    explicit operator float() const;
    explicit operator double() const;
    explicit operator long double() const;

    size_t SerializedSize() const;
    byte* Serialize( byte* buf ) const;

    // Use a template because Elemental wants two versions that take in
    // and return a byte* and a const byte* :(

    // If you change this, remember to change Serialize(),
    // SerializedSize(), and CreateBigFloatType()
    template <class T>
    T* Deserialize( T* buf )
    {
      std::memcpy( &(gmp_float.get_mpf_t()[0]._mp_size), buf, sizeof(int) );
      buf += sizeof(int);
      std::memcpy( &(gmp_float.get_mpf_t()[0]._mp_exp), buf, sizeof(mp_exp_t) );
      buf += sizeof(mp_exp_t);

      // GMP uses negative sizes to indicate sign
      std::memcpy( gmp_float.get_mpf_t()[0]._mp_d, buf,
                   std::abs(gmp_float.get_mpf_t()[0]._mp_size)*sizeof(mp_limb_t) );
      buf += gmp::num_limbs*sizeof(mp_limb_t);
      return buf;
    }
};

BigFloat operator+( const BigFloat& a, const BigFloat& b );
BigFloat operator-( const BigFloat& a, const BigFloat& b );
BigFloat operator*( const BigFloat& a, const BigFloat& b );
BigFloat operator/( const BigFloat& a, const BigFloat& b );

BigFloat operator+( const BigFloat& a, const unsigned& b );
BigFloat operator-( const BigFloat& a, const unsigned& b );
BigFloat operator*( const BigFloat& a, const unsigned& b );
BigFloat operator/( const BigFloat& a, const unsigned& b );

BigFloat operator+( const BigFloat& a, const unsigned long& b );
BigFloat operator-( const BigFloat& a, const unsigned long& b );
BigFloat operator*( const BigFloat& a, const unsigned long& b );
BigFloat operator/( const BigFloat& a, const unsigned long& b );

BigFloat operator+( const BigFloat& a, const int& b );
BigFloat operator-( const BigFloat& a, const int& b );
BigFloat operator*( const BigFloat& a, const int& b );
BigFloat operator/( const BigFloat& a, const int& b );

BigFloat operator+( const BigFloat& a, const long int& b );
BigFloat operator-( const BigFloat& a, const long int& b );
BigFloat operator*( const BigFloat& a, const long int& b );
BigFloat operator/( const BigFloat& a, const long int& b );

BigFloat operator+( const BigFloat& a, const float& b );
BigFloat operator-( const BigFloat& a, const float& b );
BigFloat operator*( const BigFloat& a, const float& b );
BigFloat operator/( const BigFloat& a, const float& b );

BigFloat operator+( const BigFloat& a, const double& b );
BigFloat operator-( const BigFloat& a, const double& b );
BigFloat operator*( const BigFloat& a, const double& b );
BigFloat operator/( const BigFloat& a, const double& b );

BigFloat operator+( const unsigned& a, const BigFloat& b );
BigFloat operator-( const unsigned& a, const BigFloat& b );
BigFloat operator*( const unsigned& a, const BigFloat& b );
BigFloat operator/( const unsigned& a, const BigFloat& b );

BigFloat operator+( const unsigned long& a, const BigFloat& b );
BigFloat operator-( const unsigned long& a, const BigFloat& b );
BigFloat operator*( const unsigned long& a, const BigFloat& b );
BigFloat operator/( const unsigned long& a, const BigFloat& b );

BigFloat operator+( const int& a, const BigFloat& b );
BigFloat operator-( const int& a, const BigFloat& b );
BigFloat operator*( const int& a, const BigFloat& b );
BigFloat operator/( const int& a, const BigFloat& b );

BigFloat operator+( const long int& a, const BigFloat& b );
BigFloat operator-( const long int& a, const BigFloat& b );
BigFloat operator*( const long int& a, const BigFloat& b );
BigFloat operator/( const long int& a, const BigFloat& b );

BigFloat operator+( const float& a, const BigFloat& b );
BigFloat operator-( const float& a, const BigFloat& b );
BigFloat operator*( const float& a, const BigFloat& b );
BigFloat operator/( const float& a, const BigFloat& b );

BigFloat operator+( const double& a, const BigFloat& b );
BigFloat operator-( const double& a, const BigFloat& b );
BigFloat operator*( const double& a, const BigFloat& b );
BigFloat operator/( const double& a, const BigFloat& b );

BigFloat operator<<( const BigFloat& a, const int& b );
BigFloat operator<<( const BigFloat& a, const long int& b );
BigFloat operator<<( const BigFloat& a, const unsigned& b );
BigFloat operator<<( const BigFloat& a, const unsigned long& b );

BigFloat operator>>( const BigFloat& a, const int& b );
BigFloat operator>>( const BigFloat& a, const long int& b );
BigFloat operator>>( const BigFloat& a, const unsigned& b );
BigFloat operator>>( const BigFloat& a, const unsigned long& b );


bool operator<( const BigFloat& a, const BigFloat& b );
bool operator>( const BigFloat& a, const BigFloat& b );
bool operator<=( const BigFloat& a, const BigFloat& b );
bool operator>=( const BigFloat& a, const BigFloat& b );
bool operator==( const BigFloat& a, const BigFloat& b );
bool operator!=( const BigFloat& a, const BigFloat& b );

std::ostream& operator<<( std::ostream& os, const BigFloat& alpha );
std::istream& operator>>( std::istream& is,       BigFloat& alpha );

} // namespace El
