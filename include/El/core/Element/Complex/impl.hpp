/*
   Copyright (c) 2009-2016, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
#ifndef EL_ELEMENT_COMPLEX_IMPL_HPP
#define EL_ELEMENT_COMPLEX_IMPL_HPP

namespace El {

// c := a / b using the textbook algorithm
template<typename Real,typename=EnableIf<IsReal<Real>>>
void NaiveDiv
( const Real& aReal, const Real& aImag,
  const Real& bReal, const Real& bImag,
        Real& cReal,       Real& cImag )
{
    const Real den = bReal*bReal + bImag*bImag;
    cReal = (aReal*bReal + aImag*bImag) / den;
    cImag = (aImag*bReal - aReal*bImag) / den;
}

// c := a / b using Smith's algorithm
// See Fig. 3 from Baudin and Smith
template<typename Real,typename=EnableIf<IsReal<Real>>>
void SmithDiv
( const Real& aReal, const Real& aImag,
  const Real& bReal, const Real& bImag,
        Real& cReal,       Real& cImag )
{
    if( Abs(bImag) <= Abs(bReal) )
    {
        const Real r = bImag/bReal;
        const Real den = bReal + bImag*r;
        cReal = (aReal + aImag*r) / den;
        cImag = (aImag - aReal*r) / den;
    }
    else
    {
        const Real r = bReal/bImag; 
        const Real den = bReal*r + bImag;
        cReal = (aReal*r + aImag) / den;
        cImag = (aImag*r - aReal) / den;
    }
}

namespace safe_div {

template<typename Real,typename=EnableIf<IsReal<Real>>>
void InternalRealPart
( const Real& aReal, const Real& aImag,
  const Real& bReal, const Real& bImag,
  const Real& r,
  const Real& t,
        Real& result )
{
    const Real zero = Real(0);
    if( r != zero )
    {
        Real br = aImag*r;
        if( br != zero )
        {
            result = (aReal + br)*t;
        }
        else
        {
            result = aReal*t + (aImag*t)*r;
        }
    }
    else
    {
        result = (aReal + bImag*(aImag/bReal))*t;
    }
}

template<typename Real,typename=EnableIf<IsReal<Real>>>
void Subinternal
( const Real& aReal, const Real& aImag,
  const Real& bReal, const Real& bImag,
        Real& cReal,       Real& cImag )
{
    Real r = bImag/bReal;
    Real t = 1/(bReal + bImag*r);
    safe_div::InternalRealPart(aReal,aImag,bReal,bImag,r,t,cReal);
    safe_div::InternalRealPart(aImag,-aReal,bReal,bImag,r,t,cImag);
}

template<typename Real,typename=EnableIf<IsReal<Real>>>
void Internal
( const Real& aReal, const Real& aImag,
  const Real& bReal, const Real& bImag,
        Real& cReal,       Real& cImag )
{
    if( Abs(bImag) <= Abs(bReal) )
    {
        safe_div::Subinternal(aReal,aImag,bReal,bImag,cReal,cImag);
    }
    else
    {
        safe_div::Subinternal(aImag,aReal,bImag,bReal,cReal,cImag);
        cImag = -cImag;
    }
}

} // namespace safe_div

template<typename Real,typename=EnableIf<IsReal<Real>>>
void SafeDiv
( const Real& aReal, const Real& aImag,
  const Real& bReal, const Real& bImag,
        Real& cReal,       Real& cImag )
{
    const Real aMax = Max( Abs(aReal), Abs(aImag) );
    const Real bMax = Max( Abs(bReal), Abs(bImag) );
    const Real beta = 2;
    const Real overflow = limits::Max<Real>();
    const Real underflow = limits::SafeMin<Real>();
    const Real eps = limits::Epsilon<Real>();
    const Real betaEpsSq = beta / (eps*eps);

    Real sigma=1;
    Real aRealScaled=aReal, aImagScaled=aImag,
         bRealScaled=bReal, bImagScaled=bImag;
    if( aMax >= overflow/2 )
    {
        aRealScaled /= 2;
        aImagScaled /= 2;
        sigma *= 2;
    }
    if( bMax >= overflow/2 )
    {
        bRealScaled /= 2;
        bImagScaled /= 2;
        sigma /= 2;
    }
    if( aMax <= underflow*beta/eps )
    {
        aRealScaled *= betaEpsSq;
        aImagScaled *= betaEpsSq;
        sigma /= betaEpsSq;
    }
    if( bMax <= underflow*beta/eps )
    {
        bRealScaled *= betaEpsSq;
        bImagScaled *= betaEpsSq;
        sigma *= betaEpsSq;
    }

    safe_div::Internal
    ( aRealScaled, aImagScaled,
      bRealScaled, bImagScaled,
      cReal,       cImag );
    cReal *= sigma;
    cImag *= sigma;
}

// Complex<float>
// ==============
template<typename S>
Complex<float>::Complex( const S& a )
: std::complex<float>(float(a))
{ }

template<typename S>
Complex<float>::Complex( const Complex<S>& a )
: std::complex<float>(float(a.real()),float(a.imag()))
{ }

template<typename S,typename T>
Complex<float>::Complex( const S& a, const T& b )
: std::complex<float>(float(a),float(b))
{ }

Complex<float>::Complex()
: std::complex<float>()
{ }
Complex<float>::Complex( const std::complex<float>& a )
: std::complex<float>(a)
{ }

// Complex<double>
// ===============
template<typename S>
Complex<double>::Complex( const S& a )
: std::complex<double>(double(a))
{ }

template<typename S>
Complex<double>::Complex( const Complex<S>& a )
: std::complex<double>(double(a.real()),double(a.imag()))
{ }

template<typename S,typename T>
Complex<double>::Complex( const S& a, const T& b )
: std::complex<double>(double(a),double(b))
{ }

Complex<double>::Complex()
: std::complex<double>()
{ }
Complex<double>::Complex( const std::complex<double>& a )
: std::complex<double>(a)
{ }

#ifdef EL_HAVE_QUAD
// Complex<Quad>
// =============
template<typename S>
Complex<Quad>::Complex( const S& a )
: std::complex<Quad>(Quad(a))
{ }

template<typename S>
Complex<Quad>::Complex( const Complex<S>& a )
: std::complex<Quad>(Quad(a.real()),Quad(a.imag()))
{ }

template<typename S,typename T>
Complex<Quad>::Complex( const S& a, const T& b )
: std::complex<Quad>(Quad(a),Quad(b))
{ }

Complex<Quad>::Complex()
: std::complex<Quad>()
{ }
Complex<Quad>::Complex( const std::complex<Quad>& a )
: std::complex<Quad>(a)
{ }
#endif

#ifdef EL_HAVE_QD
// TODO: Avoid redundancy between DoubleDouble and QuadDouble impl's
Complex<DoubleDouble>::Complex() { }

template<typename S>
Complex<DoubleDouble>::Complex( const S& a )
{ realPart = a; imagPart = 0; }
template<typename S>
Complex<DoubleDouble>::Complex( const Complex<S>& a )
{ realPart = a.real(); imagPart = a.imag(); }

template<typename S,typename T>
Complex<DoubleDouble>::Complex( const S& a, const T& b )
{ realPart = a; imagPart = b; }

Complex<DoubleDouble>::Complex( const Complex<DoubleDouble>& a )
{ realPart = a.realPart; imagPart = a.imagPart; }

Complex<DoubleDouble>::~Complex()
{ }

DoubleDouble Complex<DoubleDouble>::real() const
{ return realPart; }

DoubleDouble Complex<DoubleDouble>::imag() const
{ return imagPart; }

void Complex<DoubleDouble>::real( const DoubleDouble& newReal )
{ realPart = newReal; }

void Complex<DoubleDouble>::imag( const DoubleDouble& newImag )
{ imagPart = newImag; }

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator=( const S& a )
{
    realPart = a;
    imagPart = 0;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator=( const Complex<S>& a )
{
    realPart = a.real();
    imagPart = a.imag();
    return *this;
}

Complex<DoubleDouble>&
Complex<DoubleDouble>::operator=( const Complex<DoubleDouble>& a )
{
    realPart = a.realPart;
    imagPart = a.imagPart;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator+=( const S& a )
{
    realPart += a;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator+=( const Complex<S>& a )
{
    realPart += a.real();
    imagPart += a.imag();
    return *this;
}

Complex<DoubleDouble>&
Complex<DoubleDouble>::operator+=( const Complex<DoubleDouble>& a )
{
    realPart += a.realPart;
    imagPart += a.imagPart;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator-=( const S& a )
{
    realPart -= a;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator-=( const Complex<S>& a )
{
    realPart -= a.real();
    imagPart -= a.imag();
    return *this;
}

Complex<DoubleDouble>&
Complex<DoubleDouble>::operator-=( const Complex<DoubleDouble>& a )
{
    realPart -= a.realPart;
    imagPart -= a.imagPart;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator*=( const S& a )
{
    realPart *= a;
    imagPart *= a;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator*=( const Complex<S>& a )
{
    const DoubleDouble aReal = a.real();
    const DoubleDouble aImag = a.imag();

    const DoubleDouble newReal = aReal*realPart - aImag*imagPart;
    imagPart = aReal*imagPart + aImag*realPart;
    realPart = newReal;
    return *this;
}

Complex<DoubleDouble>&
Complex<DoubleDouble>::operator*=( const Complex<DoubleDouble>& a )
{
    const DoubleDouble newReal = a.realPart*realPart - a.imagPart*imagPart;
    imagPart = a.realPart*imagPart + a.imagPart*realPart;
    realPart = newReal;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator/=( const S& b )
{
    realPart /= b;
    imagPart /= b;
    return *this;
}

template<typename S>
Complex<DoubleDouble>&
Complex<DoubleDouble>::operator/=( const Complex<S>& b )
{
    // Note that GCC uses the (faster and less stable) textbook algorithm
    auto a = *this;
    SmithDiv
    ( a.realPart, a.imagPart, 
      DoubleDouble(b.real()), DoubleDouble(b.imag()),
      realPart, imagPart );
    return *this;
}

Complex<DoubleDouble>&
Complex<DoubleDouble>::operator/=( const Complex<DoubleDouble>& b )
{
    // Note that GCC uses the (faster and less stable) textbook algorithm
    auto a = *this;
    SmithDiv
    ( a.realPart, a.imagPart, 
      b.realPart, b.imagPart,
      realPart, imagPart );
    return *this;
}

Complex<QuadDouble>::Complex() { }

template<typename S>
Complex<QuadDouble>::Complex( const S& a )
{ realPart = a; imagPart = 0; }
template<typename S>
Complex<QuadDouble>::Complex( const Complex<S>& a )
{ realPart = a.real(); imagPart = a.imag(); }

template<typename S,typename T>
Complex<QuadDouble>::Complex( const S& a, const T& b )
{ realPart = a; imagPart = b; }

Complex<QuadDouble>::Complex( const Complex<QuadDouble>& a )
{ realPart = a.realPart; imagPart = a.imagPart; }

Complex<QuadDouble>::~Complex()
{ }

QuadDouble Complex<QuadDouble>::real() const
{ return realPart; }

QuadDouble Complex<QuadDouble>::imag() const
{ return imagPart; }

void Complex<QuadDouble>::real( const QuadDouble& newReal )
{ realPart = newReal; }

void Complex<QuadDouble>::imag( const QuadDouble& newImag )
{ imagPart = newImag; }

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator=( const S& a )
{
    realPart = a;
    imagPart = 0;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator=( const Complex<S>& a )
{
    realPart = a.real();
    imagPart = a.imag();
    return *this;
}

Complex<QuadDouble>&
Complex<QuadDouble>::operator=( const Complex<QuadDouble>& a )
{
    realPart = a.realPart;
    imagPart = a.imagPart;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator+=( const S& a )
{
    realPart += a;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator+=( const Complex<S>& a )
{
    realPart += a.real();
    imagPart += a.imag();
    return *this;
}

Complex<QuadDouble>&
Complex<QuadDouble>::operator+=( const Complex<QuadDouble>& a )
{
    realPart += a.realPart;
    imagPart += a.imagPart;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator-=( const S& a )
{
    realPart -= a;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator-=( const Complex<S>& a )
{
    realPart -= a.real();
    imagPart -= a.imag();
    return *this;
}

Complex<QuadDouble>&
Complex<QuadDouble>::operator-=( const Complex<QuadDouble>& a )
{
    realPart -= a.realPart;
    imagPart -= a.imagPart;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator*=( const S& a )
{
    realPart *= a;
    imagPart *= a;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator*=( const Complex<S>& a )
{
    const QuadDouble aReal = a.real();
    const QuadDouble aImag = a.imag();

    const QuadDouble newReal = aReal*realPart - aImag*imagPart;
    imagPart = aReal*imagPart + aImag*realPart;
    realPart = newReal;
    return *this;
}

Complex<QuadDouble>&
Complex<QuadDouble>::operator*=( const Complex<QuadDouble>& a )
{
    const QuadDouble newReal = a.realPart*realPart - a.imagPart*imagPart;
    imagPart = a.realPart*imagPart + a.imagPart*realPart;
    realPart = newReal;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator/=( const S& b )
{
    realPart /= b;
    imagPart /= b;
    return *this;
}

template<typename S>
Complex<QuadDouble>&
Complex<QuadDouble>::operator/=( const Complex<S>& b )
{
    // Note that GCC uses the (faster and less stable) textbook algorithm
    auto a = *this;
    SmithDiv
    ( a.realPart, a.imagPart, 
      QuadDouble(b.real()), QuadDouble(b.imag()),
      realPart, imagPart );
    return *this;
}

Complex<QuadDouble>&
Complex<QuadDouble>::operator/=( const Complex<QuadDouble>& b )
{
    // Note that GCC uses the (faster and less stable) textbook algorithm
    auto a = *this;
    SmithDiv
    ( a.realPart, a.imagPart, 
      b.realPart, b.imagPart,
      realPart, imagPart );
    return *this;
}
#endif

#ifdef EL_HAVE_GMPXX
// Complex<BigFloat>
// =================

mp_bitcnt_t Complex<BigFloat>::Precision() const
{ return gmp_real.Precision(); }

void Complex<BigFloat>::SetPrecision( mp_bitcnt_t prec )
{
    gmp_real.SetPrecision(prec);
    gmp_imag.SetPrecision(prec);
}

void Complex<BigFloat>::real( const BigFloat& newReal )
{
    gmp_real=newReal;
}

void Complex<BigFloat>::imag( const BigFloat& newImag )
{
    gmp_imag=newImag;
}

BigFloat Complex<BigFloat>::real() const
{
  return gmp_real;
}

BigFloat Complex<BigFloat>::imag() const
{
  return gmp_imag;
}

Complex<BigFloat>::Complex()
{}

template<typename S>
Complex<BigFloat>::Complex( const S& a, mp_bitcnt_t prec )
  : gmp_real(a,prec), gmp_imag(BigFloat(0),prec) {}

template<typename S>
Complex<BigFloat>::Complex( const Complex<S>& a, mp_bitcnt_t prec )
  : gmp_real(a.real(),prec), gmp_imag(a.imag(),prec) {}

Complex<BigFloat>::Complex
( const unsigned& a, mp_bitcnt_t prec )
  : gmp_real(a,prec), gmp_imag(BigFloat(0),prec) {}

Complex<BigFloat>::Complex( const int& a, mp_bitcnt_t prec )
  : gmp_real(a,prec), gmp_imag(BigFloat(0),prec) {}

Complex<BigFloat>::Complex( const float& a, mp_bitcnt_t prec )
  : gmp_real(a,prec), gmp_imag(BigFloat(0),prec) {}

Complex<BigFloat>::Complex
( const std::complex<float>& a, mp_bitcnt_t prec )
  : gmp_real(a.real(),prec), gmp_imag(a.imag(),prec) {}

Complex<BigFloat>::Complex( const double& a, mp_bitcnt_t prec )
  : gmp_real(a,prec), gmp_imag(BigFloat(0),prec) {}

Complex<BigFloat>::Complex( const std::complex<double>& a, mp_bitcnt_t prec )
  : gmp_real(a.real(),prec), gmp_imag(a.imag(),prec) {}

Complex<BigFloat>::Complex( const realType& a, mp_bitcnt_t prec )
  : gmp_real(a,prec), gmp_imag(BigFloat(0),prec) {}

Complex<BigFloat>::Complex
( const realType& a,
  const realType& b,
        mp_bitcnt_t prec )
  : gmp_real(a,prec), gmp_imag(b,prec) {}

Complex<BigFloat>::Complex( const Complex<realType>& a, mp_bitcnt_t prec )
  : gmp_real(a.real(),prec), gmp_imag(a.imag(),prec) {}

Complex<BigFloat>::Complex( Complex<realType>&& a )
  : gmp_real(a.real()), gmp_imag(a.imag()) {}

Complex<BigFloat>& Complex<BigFloat>::operator=( Complex<BigFloat>&& a )
{
    std::swap(gmp_real,a.gmp_real);
    std::swap(gmp_imag,a.gmp_imag);
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const Complex<BigFloat>& a )
{
    gmp_real=a.real();
    gmp_imag=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const BigFloat& a )
{
    gmp_real=a;
    gmp_imag=0;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator=( const S& a )
{
    gmp_real=a;
    gmp_imag=0;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator=( const Complex<S>& a )
{
    gmp_real=a.real();
    gmp_imag=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const Complex<double>& a )
{
    gmp_real=a.real();
    gmp_imag=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const double& a )
{
    gmp_real=a;
    gmp_imag=0;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const Complex<float>& a )
{
    gmp_real=a.real();
    gmp_imag=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const float& a )
{
    gmp_real=a;
    gmp_imag=0;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const long int& a )
{
    gmp_real=a;
    gmp_imag=0;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const int& a )
{
    gmp_real=a;
    gmp_imag=0;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const unsigned long& a )
{
    gmp_real=a;
    gmp_imag=0;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator=( const unsigned& a )
{
    gmp_real=a;
    gmp_imag=0;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator+=( const S& a )
{
    gmp_real+=a;
    gmp_imag=0;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator+=( const Complex<S>& a )
{
    gmp_real+=a.real();
    gmp_imag+=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const Complex<BigFloat>& a )
{
    gmp_real+=a.real();
    gmp_imag+=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const BigFloat& a )
{
    gmp_real+=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const Complex<double>& a )
{
    gmp_real+=a.real();
    gmp_imag+=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const double& a )
{
    gmp_real+=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const Complex<float>& a )
{
    gmp_real+=a.real();
    gmp_imag+=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const float& a )
{
    gmp_real+=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const long int& a )
{
    gmp_real+=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const int& a )
{
    gmp_real+=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const unsigned long& a )
{
    gmp_real+=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator+=( const unsigned& a )
{
    gmp_real+=a;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator-=( const S& a )
{
    gmp_real-=a;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator-=( const Complex<S>& a )
{
    gmp_real-=a.real();
    gmp_imag-=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const Complex<BigFloat>& a )
{
    gmp_real-=a.real();
    gmp_imag-=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const BigFloat& a )
{
    gmp_real-=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const Complex<double>& a )
{
    gmp_real-=a.real();
    gmp_imag-=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const double& a )
{
    gmp_real-=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const Complex<float>& a )
{
    gmp_real-=a.real();
    gmp_imag-=a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const float& a )
{
    gmp_real-=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const long int& a )
{
    gmp_real-=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const int& a )
{
    gmp_real-=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const unsigned long& a )
{
    gmp_real-=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator-=( const unsigned& a )
{
    gmp_real-=a;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator*=( const S& a )
{
    gmp_real*=a;
    gmp_imag*=a;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator*=( const Complex<S>& a )
{
    gmp_real=gmp_real*a.real() - gmp_imag*a.imag();
    gmp_imag=gmp_imag*a.real() + gmp_real*a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const Complex<BigFloat>& a )
{
    gmp_real=gmp_real*a.real() - gmp_imag*a.imag();
    gmp_imag=gmp_imag*a.real() + gmp_real*a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const BigFloat& a )
{
    gmp_real*=a;
    gmp_imag*=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const Complex<double>& a )
{
    gmp_real=gmp_real*a.real() - gmp_imag*a.imag();
    gmp_imag=gmp_imag*a.real() + gmp_real*a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const double& a )
{
    gmp_real*=a;
    gmp_imag*=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const Complex<float>& a )
{
    gmp_real=gmp_real*a.real() - gmp_imag*a.imag();
    gmp_imag=gmp_imag*a.real() + gmp_real*a.imag();
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const float& a )
{
    gmp_real*=a;
    gmp_imag*=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const long int& a )
{
    gmp_real*=a;
    gmp_imag*=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const int& a )
{
    gmp_real*=a;
    gmp_imag*=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const unsigned long& a )
{
    gmp_real*=a;
    gmp_imag*=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator*=( const unsigned& a )
{
    gmp_real*=a;
    gmp_imag*=a;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator/=( const Complex<S>& a )
{
    BigFloat denominator((a.real()*a.real() + a.imag()*a.imag()));
    gmp_real=(gmp_real*a.real() + gmp_imag*a.imag())/denominator;
    gmp_imag=(gmp_imag*a.real() - gmp_real*a.imag())/denominator;
    return *this;
}

template<typename S>
Complex<BigFloat>& Complex<BigFloat>::operator/=( const S& a )
{
    gmp_real/=a;
    gmp_imag/=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const Complex<BigFloat>& a )
{
    BigFloat denominator((a.real()*a.real() + a.imag()*a.imag()));
    BigFloat real_result((gmp_real*a.real() + gmp_imag*a.imag())/denominator);
    BigFloat imag_result((gmp_imag*a.real() - gmp_real*a.imag())/denominator);
    gmp_real=real_result;
    gmp_imag=imag_result;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const BigFloat& a )
{
    gmp_real/=a;
    gmp_imag/=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const Complex<double>& a )
{
    BigFloat denominator((a.real()*a.real() + a.imag()*a.imag()));
    BigFloat real_result((gmp_real*a.real() + gmp_imag*a.imag())/denominator);
    BigFloat imag_result((gmp_imag*a.real() - gmp_real*a.imag())/denominator);
    gmp_real=real_result;
    gmp_imag=imag_result;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const double& a )
{
    gmp_real/=a;
    gmp_imag/=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const Complex<float>& a )
{
    BigFloat denominator((a.real()*a.real() + a.imag()*a.imag()));
    BigFloat real_result((gmp_real*a.real() + gmp_imag*a.imag())/denominator);
    BigFloat imag_result((gmp_imag*a.real() - gmp_real*a.imag())/denominator);
    gmp_real=real_result;
    gmp_imag=imag_result;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const float& a )
{
    gmp_real/=a;
    gmp_imag/=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const long int& a )
{
    gmp_real/=a;
    gmp_imag/=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const int& a )
{
    gmp_real/=a;
    gmp_imag/=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const unsigned long& a )
{
    gmp_real/=a;
    gmp_imag/=a;
    return *this;
}

Complex<BigFloat>& Complex<BigFloat>::operator/=( const unsigned& a )
{
    gmp_real/=a;
    gmp_imag/=a;
    return *this;
}

void Complex<BigFloat>::Zero()
{
    gmp_real=0;
    gmp_imag=0;
}

size_t Complex<BigFloat>::SerializedSize() const
{
    return gmp_real.SerializedSize() + gmp_imag.SerializedSize();
}

byte* Complex<BigFloat>::Serialize( byte* buf ) const
{
    buf=gmp_real.Serialize(buf);
    buf=gmp_imag.Serialize(buf);
    return buf;
}

#endif

#ifdef EL_HAVE_QD
bool operator==
( const Complex<DoubleDouble>& a, const Complex<DoubleDouble>& b )
{
    return (a.real() == b.real()) && (a.imag() == b.imag());
}

bool operator!=
( const Complex<DoubleDouble>& a, const Complex<DoubleDouble>& b )
{
    return !(a == b);
}

bool operator==
( const Complex<QuadDouble>& a, const Complex<QuadDouble>& b )
{
    return (a.real() == b.real()) && (a.imag() == b.imag());
}

bool operator!=
( const Complex<QuadDouble>& a, const Complex<QuadDouble>& b )
{
    return !(a == b);
}

bool operator==
( const Complex<DoubleDouble>& a, const DoubleDouble& b )
{
    return (a.real() == b) && (a.imag() == DoubleDouble(0));
}

bool operator!=
( const Complex<DoubleDouble>& a, const DoubleDouble& b )
{
    return !(a == b);
}

bool operator==
( const Complex<QuadDouble>& a, const QuadDouble& b )
{
    return (a.real() == b) && (a.imag() == QuadDouble(0));
}

bool operator!=
( const Complex<QuadDouble>& a, const QuadDouble& b )
{
    return !(a == b);
}

bool operator==
( const DoubleDouble& a, const Complex<DoubleDouble>& b )
{
    return (a == b.real()) && (DoubleDouble(0) == b.imag());
}

bool operator!=
( const DoubleDouble& a, const Complex<DoubleDouble>& b )
{
    return !(a == b);
}

bool operator==
( const QuadDouble& a, const Complex<QuadDouble>& b )
{
    return (a == b.real()) && (QuadDouble(0) == b.imag());
}

bool operator!=
( const QuadDouble& a, const Complex<QuadDouble>& b )
{
    return !(a == b);
}
#endif
#ifdef EL_HAVE_GMPXX
bool operator==
( const Complex<BigFloat>& a, const Complex<BigFloat>& b )
{
    return a.real() == b.real() && a.imag() == b.imag();
}

bool operator!=
( const Complex<BigFloat>& a, const Complex<BigFloat>& b )
{
    return !(a == b);
}

bool operator==
( const Complex<BigFloat>& a, const BigFloat& b )
{
    return a.real() == b && a.imag() == BigFloat(0.0);
}

bool operator!=
( const Complex<BigFloat>& a, const BigFloat& b )
{
    return !(a == b);
}

bool operator==
( const BigFloat& a, const Complex<BigFloat>& b )
{
    return b.real() == a && b.imag() == BigFloat(0.0);
}

bool operator!=
( const BigFloat& a, const Complex<BigFloat>& b )
{
    return !(a == b);
}
#endif

template<typename Real>
Complex<Real>
operator-( const Complex<Real>& a )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    return -aStd;
}
#ifdef EL_HAVE_QD
Complex<DoubleDouble> operator-( const Complex<DoubleDouble>& a )
{
    Complex<DoubleDouble> aNeg;
    aNeg.realPart = -a.realPart;
    aNeg.imagPart = -a.imagPart;
    return aNeg;
}
Complex<QuadDouble> operator-( const Complex<QuadDouble>& a )
{
    Complex<QuadDouble> aNeg;
    aNeg.realPart = -a.realPart;
    aNeg.imagPart = -a.imagPart;
    return aNeg;
}
#endif
#ifdef EL_HAVE_GMPXX
Complex<BigFloat> operator-( const Complex<BigFloat>& a )
{
    Complex<BigFloat> aNeg;
    aNeg.real(-a.real());
    aNeg.imag(-a.imag());
    return aNeg;
}
#endif

template<typename Real>
Complex<Real>
operator+( const Complex<Real>& a, const Complex<Real>& b )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    auto& bStd = static_cast<const std::complex<Real>&>(b);
    return aStd + bStd;
}
template<typename Real>
Complex<Real>
operator-( const Complex<Real>& a, const Complex<Real>& b )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    auto& bStd = static_cast<const std::complex<Real>&>(b);
    return aStd - bStd;
}
template<typename Real>
Complex<Real>
operator*( const Complex<Real>& a, const Complex<Real>& b )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    auto& bStd = static_cast<const std::complex<Real>&>(b);
    return aStd * bStd;
}
template<typename Real>
Complex<Real>
operator/( const Complex<Real>& a, const Complex<Real>& b )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    auto& bStd = static_cast<const std::complex<Real>&>(b);
    return aStd / bStd;
}
#ifdef EL_HAVE_QD
Complex<DoubleDouble> operator+
( const Complex<DoubleDouble>& a, const Complex<DoubleDouble>& b )
{
    Complex<DoubleDouble> c(a);
    c += b;
    return c;
}
Complex<DoubleDouble> operator-
( const Complex<DoubleDouble>& a, const Complex<DoubleDouble>& b )
{
    Complex<DoubleDouble> c(a);
    c -= b;
    return c;
}
Complex<DoubleDouble> operator*
( const Complex<DoubleDouble>& a, const Complex<DoubleDouble>& b )
{
    Complex<DoubleDouble> c(a);
    c *= b;
    return c;
}
Complex<DoubleDouble> operator/
( const Complex<DoubleDouble>& a, const Complex<DoubleDouble>& b )
{
    Complex<DoubleDouble> c;
    SmithDiv
    ( a.realPart, a.imagPart,
      b.realPart, b.imagPart,
      c.realPart, c.imagPart );
    return c;
}

Complex<QuadDouble> operator+
( const Complex<QuadDouble>& a, const Complex<QuadDouble>& b )
{
    Complex<QuadDouble> c(a);
    c += b;
    return c;
}
Complex<QuadDouble> operator-
( const Complex<QuadDouble>& a, const Complex<QuadDouble>& b )
{
    Complex<QuadDouble> c(a);
    c -= b;
    return c;
}
Complex<QuadDouble> operator*
( const Complex<QuadDouble>& a, const Complex<QuadDouble>& b )
{
    Complex<QuadDouble> c(a);
    c *= b;
    return c;
}
Complex<QuadDouble> operator/
( const Complex<QuadDouble>& a, const Complex<QuadDouble>& b )
{
    Complex<QuadDouble> c;
    SmithDiv
    ( a.realPart, a.imagPart,
      b.realPart, b.imagPart,
      c.realPart, c.imagPart );
    return c;
}
#endif
#ifdef EL_HAVE_GMPXX
Complex<BigFloat> operator+
( const Complex<BigFloat>& a, const Complex<BigFloat>& b )
{
    Complex<BigFloat> c(a);
    c+=b;
    return c;
}
Complex<BigFloat> operator-
( const Complex<BigFloat>& a, const Complex<BigFloat>& b )
{
    Complex<BigFloat> c(a);
    c-=b;
    return c;
}
Complex<BigFloat> operator*
( const Complex<BigFloat>& a, const Complex<BigFloat>& b )
{
    Complex<BigFloat> c(a);
    c*=b;
    return c;
}
Complex<BigFloat> operator/
( const Complex<BigFloat>& a, const Complex<BigFloat>& b )
{
    Complex<BigFloat> c(a);
    c/=b;
    return c;
}
#endif

template<typename Real>
Complex<Real>
operator+( const Complex<Real>& a, const Real& b )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    return aStd + b;
}
template<typename Real>
Complex<Real>
operator-( const Complex<Real>& a, const Real& b )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    return aStd - b;
}
template<typename Real>
Complex<Real>
operator*( const Complex<Real>& a, const Real& b )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    return aStd * b;
}
template<typename Real>
Complex<Real>
operator/( const Complex<Real>& a, const Real& b )
{
    auto& aStd = static_cast<const std::complex<Real>&>(a);
    return aStd / b;
}
#ifdef EL_HAVE_QD
Complex<DoubleDouble> operator+
( const Complex<DoubleDouble>& a, const DoubleDouble& b )
{
    Complex<DoubleDouble> c(a);
    c += b;
    return c;
}
Complex<DoubleDouble> operator-
( const Complex<DoubleDouble>& a, const DoubleDouble& b )
{
    Complex<DoubleDouble> c(a);
    c -= b;
    return c;
}
Complex<DoubleDouble> operator*
( const Complex<DoubleDouble>& a, const DoubleDouble& b )
{
    Complex<DoubleDouble> c(a);
    c *= b;
    return c;
}
Complex<DoubleDouble> operator/
( const Complex<DoubleDouble>& a, const DoubleDouble& b )
{
    Complex<DoubleDouble> c(a);
    c.realPart /= b;
    c.imagPart /= b;
    return c;
}

Complex<QuadDouble> operator+
( const Complex<QuadDouble>& a, const QuadDouble& b )
{
    Complex<QuadDouble> c(a);
    c += b;
    return c;
}
Complex<QuadDouble> operator-
( const Complex<QuadDouble>& a, const QuadDouble& b )
{
    Complex<QuadDouble> c(a);
    c -= b;
    return c;
}
Complex<QuadDouble> operator*
( const Complex<QuadDouble>& a, const QuadDouble& b )
{
    Complex<QuadDouble> c(a);
    c *= b;
    return c;
}
Complex<QuadDouble> operator/
( const Complex<QuadDouble>& a, const QuadDouble& b )
{
    Complex<QuadDouble> c(a);
    c.realPart /= b;
    c.imagPart /= b;
    return c;
}
#endif
#ifdef EL_HAVE_GMPXX

Complex<BigFloat> operator+
( const Complex<BigFloat>& a, const BigFloat& b )
{
    Complex<BigFloat> c(a);
    c+=b;
    return c;
}
Complex<BigFloat> operator-
( const Complex<BigFloat>& a, const BigFloat& b )
{
    Complex<BigFloat> c(a);
    c-=b;
    return c;
}
Complex<BigFloat> operator*
( const Complex<BigFloat>& a, const BigFloat& b )
{
    Complex<BigFloat> c(a);
    c*=b;
    return c;
}
Complex<BigFloat> operator/
( const Complex<BigFloat>& a, const BigFloat& b )
{
    Complex<BigFloat> c(a);
    c/=b;
    return c;
}
#endif

template<typename Real>
Complex<Real>
operator+( const Real& a, const Complex<Real>& b )
{
    auto& bStd = static_cast<const std::complex<Real>&>(b);
    return a + bStd;
}
template<typename Real>
Complex<Real>
operator-( const Real& a, const Complex<Real>& b )
{
    auto& bStd = static_cast<const std::complex<Real>&>(b);
    return a - bStd;
}
template<typename Real>
Complex<Real>
operator*( const Real& a, const Complex<Real>& b )
{
    auto& bStd = static_cast<const std::complex<Real>&>(b);
    return a * bStd;
}
template<typename Real>
Complex<Real>
operator/( const Real& a, const Complex<Real>& b )
{
    auto& bStd = static_cast<const std::complex<Real>&>(b);
    return a / bStd;
}
#ifdef EL_HAVE_QD
Complex<DoubleDouble> operator+
( const DoubleDouble& a, const Complex<DoubleDouble>& b )
{
    Complex<DoubleDouble> c(b);
    c += a;
    return c;
}
Complex<DoubleDouble> operator-
( const DoubleDouble& a, const Complex<DoubleDouble>& b )
{
    Complex<DoubleDouble> c;
    c.realPart = a - b.realPart;
    c.imagPart =   - b.imagPart;
    return c;
}
Complex<DoubleDouble> operator*
( const DoubleDouble& a, const Complex<DoubleDouble>& b )
{
    Complex<DoubleDouble> c(b);
    c *= a;
    return c;
}
Complex<DoubleDouble> operator/
( const DoubleDouble& a, const Complex<DoubleDouble>& b )
{
    Complex<DoubleDouble> c;
    SmithDiv
    ( a, DoubleDouble(0),
      b.realPart, b.imagPart,
      c.realPart, c.imagPart );
    return c;
}

Complex<QuadDouble> operator+
( const QuadDouble& a, const Complex<QuadDouble>& b )
{
    Complex<QuadDouble> c(b);
    c += a;
    return c;
}
Complex<QuadDouble> operator-
( const QuadDouble& a, const Complex<QuadDouble>& b )
{
    Complex<QuadDouble> c;
    c.realPart = a - b.realPart;
    c.imagPart =   - b.imagPart;
    return c;
}
Complex<QuadDouble> operator*
( const QuadDouble& a, const Complex<QuadDouble>& b )
{
    Complex<QuadDouble> c(b);
    c *= a;
    return c;
}
Complex<QuadDouble> operator/
( const QuadDouble& a, const Complex<QuadDouble>& b )
{
    Complex<QuadDouble> c;
    SmithDiv
    ( a, QuadDouble(0),
      b.realPart, b.imagPart,
      c.realPart, c.imagPart );
    return c;
}
#endif
#ifdef EL_HAVE_GMPXX
Complex<BigFloat> operator+
( const BigFloat& a, const Complex<BigFloat>& b )
{
    Complex<BigFloat> c(a);
    c+=b;
    return c;
}
Complex<BigFloat> operator-
( const BigFloat& a, const Complex<BigFloat>& b )
{
    Complex<BigFloat> c(a);
    c-=b;
    return c;
}
Complex<BigFloat> operator*
( const BigFloat& a, const Complex<BigFloat>& b )
{
    Complex<BigFloat> c(a);
    c*=b;
    return c;
}
Complex<BigFloat> operator/
( const BigFloat& a, const Complex<BigFloat>& b )
{
    Complex<BigFloat> c(a);
    c/=b;
    return c;
}
#endif

template<typename Real,typename>
Real NaiveDiv( const Real& a, const Real& b )
{ return a / b; }
template<typename Real,typename>
Complex<Real> NaiveDiv
( const Real& a,
  const Complex<Real>& b )
{
    Real cReal, cImag;
    NaiveDiv( a, Real(0), b.real(), b.imag(), cReal, cImag );
    return Complex<Real>(cReal,cImag);
}
template<typename Real,typename>
Complex<Real> NaiveDiv
( const Complex<Real>& a,
  const Real& b )
{ return a / b; }
template<typename Real,typename>
Complex<Real> NaiveDiv
( const Complex<Real>& a,
  const Complex<Real>& b )
{
    Real cReal, cImag;
    NaiveDiv( a.real(), a.imag(), b.real(), b.imag(), cReal, cImag );
    return Complex<Real>(cReal,cImag);
}

template<typename Real,typename>
Real SmithDiv( const Real& a, const Real& b )
{ return a / b; }
template<typename Real,typename>
Complex<Real> SmithDiv
( const Real& a,
  const Complex<Real>& b )
{
    Real cReal, cImag;
    SmithDiv( a, Real(0), b.real(), b.imag(), cReal, cImag );
    return Complex<Real>(cReal,cImag);
}
template<typename Real,typename>
Complex<Real> SmithDiv
( const Complex<Real>& a,
  const Real& b )
{ return a / b; }
template<typename Real,typename>
Complex<Real> SmithDiv
( const Complex<Real>& a,
  const Complex<Real>& b )
{
    Real cReal, cImag;
    SmithDiv( a.real(), a.imag(), b.real(), b.imag(), cReal, cImag );
    return Complex<Real>(cReal,cImag);
}

template<typename Real,typename>
Real SafeDiv( const Real& a, const Real& b )
{ return a / b; }
template<typename Real,typename>
Complex<Real> SafeDiv
( const Real& a,
  const Complex<Real>& b )
{
    Real cReal, cImag;
    SafeDiv( a, Real(0), b.real(), b.imag(), cReal, cImag );
    return Complex<Real>(cReal,cImag);
}
template<typename Real,typename>
Complex<Real> SafeDiv
( const Complex<Real>& a,
  const Real& b )
{ return a / b; }
template<typename Real,typename>
Complex<Real> SafeDiv
( const Complex<Real>& a,
  const Complex<Real>& b )
{
    Real cReal, cImag;
    SafeDiv( a.real(), a.imag(), b.real(), b.imag(), cReal, cImag );
    return Complex<Real>(cReal,cImag);
}

} // namespace El

#endif // ifndef EL_ELEMENT_COMPLEX_IMPL_HPP
