/*
   Copyright (c) 2009-2016, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
#include <El.hpp>
#include <limits>
using namespace El;

template<typename Real>
void QueryLimits( const std::string& type_name )
{
    Output( type_name );
    Output("  base:      ",limits::Base<Real>());
    if constexpr (!std::numeric_limits<int>::is_integer) {
        Output("  epsilon:   ",limits::Epsilon<Real>());
        Output("  precision: ",limits::Precision<Real>());
        Output("  safeMin:   ",limits::SafeMin<Real>());
        const auto safeInv = limits::SafeMin<Real>() / limits::Epsilon<Real>();
        Output("  safeInv:   ", safeInv);
    }
    Output("  min:       ",limits::Min<Real>());
    Output("  min/2:     ",limits::Min<Real>()/Real(2));
    Output("  max:       ",limits::Max<Real>());
    Output("  max*2:     ",limits::Max<Real>()*Real(2));
    Output("  lowest:    ",limits::Lowest<Real>());
    Output("  infinity:  ",limits::Infinity<Real>());
    Output("");

#define COMPARE_WITH_STL(El_func, std_func) do { \
      const Real std_limit = std::numeric_limits<Real>::std_func(); \
      const Real El_limit = limits::El_func<Real>(); \
      if (std_limit != El_limit) \
        RuntimeError("Error for Real=", type_name, ": std::numeric_limits<Real>::", #std_func,"() = ", std_limit, \
        ", El::limits::", #El_func,"<Real>() = ", El_limit, \
        ", diff = ", std_limit - El_limit); \
    } while(false)

    if (std::numeric_limits<Real>::is_specialized) {
        COMPARE_WITH_STL(Min, min);
        COMPARE_WITH_STL(Max, max);
        COMPARE_WITH_STL(Lowest, lowest);
        COMPARE_WITH_STL(Infinity, infinity);
        // see comment for Precision() in El/core/limits.hpp
        COMPARE_WITH_STL(Precision, epsilon);
#undef COMPARE_WITH_STL
    }
}

int 
main( int argc, char* argv[] )
{
    Environment env( argc, argv );
    const Int commRank = mpi::Rank();

    if( commRank == 0 )
    {
        QueryLimits<float>( "float" );
        QueryLimits<double>( "double" );
        QueryLimits<int>( "int" );
        QueryLimits<unsigned int>( "unsigned int" );
#ifdef EL_HAVE_QD
        QueryLimits<DoubleDouble>( "DoubleDouble" );
        QueryLimits<QuadDouble>( "QuadDouble" );
#endif
#ifdef EL_HAVE_QUAD
        QueryLimits<Quad>( "Quad-precision" );
#endif
#ifdef EL_HAVE_MPC
        QueryLimits<BigFloat>( "BigFloat(Default)" );
        mpfr::SetPrecision( 64 );
        QueryLimits<BigFloat>( "BigFloat(64)" );
        mpfr::SetPrecision( 128 );
        QueryLimits<BigFloat>( "BigFloat(128)" );
        mpfr::SetPrecision( 256 );
        QueryLimits<BigFloat>( "BigFloat(256)" );
        mpfr::SetPrecision( 512 );
        QueryLimits<BigFloat>( "BigFloat(512)" );
        mpfr::SetPrecision( 1024 );
        QueryLimits<BigFloat>( "BigFloat(1024)" );
#endif
#ifdef EL_HAVE_GMPXX
        QueryLimits<BigFloat>( "BigFloat(Default)" );
        El::gmp::SetPrecision( 1024 );
        QueryLimits<BigFloat>( "BigFloat(1024)" );
        // TODO El::gmp::SetPrecision() throws an error if called twice, so we cannot test more values
#endif
    }

    return 0;
}
