/*
   Copyright (c) 2009-2016, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License,
   which can be found in the LICENSE file in the root directory, or at
   http://opensource.org/licenses/BSD-2-Clause
*/
#include <El.hpp>

#define ASSERT_EQUAL(a,b) do{ \
    const auto x = a; \
    const auto y = b; \
    if(x != y) \
      RuntimeError(#a, " != ", #b, ": ", #a, " = ", x, ", " #b, " = ", y, ", typeid(Real) = \"", typeid(x).name(),"\""); \
  } while(false)

using namespace El;

template<typename Real, typename TestMatrix>
void TestMax(Int m)
{
    EL_DEBUG_ONLY(CallStackEntry cse("TestMax"))
    TestMatrix A;
    Zeros(A, m, m);
    ASSERT_EQUAL(Max(A), Real(0));
    Ones(A, m, m);
    ASSERT_EQUAL(Max(A), Real(1));
    Scale(Real(5), A);
    ASSERT_EQUAL(Max(A), Real(5));
    Scale(Real(-5), A);
    ASSERT_EQUAL(Max(A), Real(-25));
    MinIJ(A, m);
    ASSERT_EQUAL(Max(A), Real(m));
    OneTwoOne(A, m);
    ASSERT_EQUAL(Max(A), Real(2));
}

template<typename Real, typename TestMatrix>
void TestSymmetricMax(Int m)
{
    EL_DEBUG_ONLY(CallStackEntry cse("TestSymmetricMax"))
    TestMatrix A;
    Zeros(A, m, m);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::LOWER, A), Real(0));
    Ones(A, m, m);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::LOWER, A), Real(1));
    Scale(Real(5), A);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::LOWER, A), Real(5));
    Scale(Real(-5), A);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::LOWER, A), Real(-25));
    MinIJ(A, m);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::LOWER, A), Real(m));
    OneTwoOne(A, m);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::LOWER, A), Real(2));
    Zeros(A, m, m);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::UPPER, A), Real(0));
    Ones(A, m, m);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::UPPER, A), Real(1));
    Scale(Real(5), A);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::UPPER, A), Real(5));
    Scale(Real(-5), A);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::UPPER, A), Real(-25));
    MinIJ(A, m);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::UPPER, A), Real(m));
    OneTwoOne(A, m);
    ASSERT_EQUAL(SymmetricMax(UpperOrLower::UPPER, A), Real(2));
}

void RunTests(Int m)
{
#define RUN_TESTS(type) \
    do { \
        TestMax<type,DistMatrix<type>>(m); \
        TestMax<type,Matrix<type>>(m); \
        TestSymmetricMax<type,DistMatrix<type>>(m); \
        TestSymmetricMax<type,Matrix<type>>(m); \
    } while(false)

  RUN_TESTS(double);
  RUN_TESTS(float);
  RUN_TESTS(BigFloat);
  RUN_TESTS(int);

#undef RUN_TESTS
}

int main(int argc, char* argv[])
{
    Environment env(argc, argv);
    // Without this line, MPI dataypte for BigFloat is not initialized:
    gmp::SetPrecision(gmp::Precision());
    try
    {
        Int m = 1;
        for(Int e = 1; e<4; ++e)
        {
            m *= 10;
            Output(" m = ",m,"\n");
            RunTests(m);
        }
    }
    catch(exception& e) { ReportException(e); }
    return 0;
}
